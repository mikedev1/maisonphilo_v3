<?php

namespace App\Controller;

use App\Repository\ServicesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public $servicesRepository;

    public function __construct(ServicesRepository $servicesRepository)
    {
        $this->servicesRepository = $servicesRepository;
    }

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $services = $this->servicesRepository->findBy([],["position" => "ASC"]);
        return $this->render('home/index.html.twig', [
            'services' => $services,
        ]);
    }
}
