<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SharingInformationController extends AbstractController
{
    #[Route('/sharing/information', name: 'app_sharing_information')]
    public function index(): Response
    {
        return $this->render('sharing_information/index.html.twig', [
            'controller_name' => 'SharingInformationController',
        ]);
    }
}
