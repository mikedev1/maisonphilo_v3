<?php

namespace App\DataFixtures;

use App\Entity\Services;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $services = [
            [
                "name" => "Hôpitaux & Centres médicaux",
                "isValid" => true,
                "position" => 1
            ],
            [
                "name" => "Annuaire associations",
                "isValid" => true,
                "position" => 1
            ],
            [
                "name" => "Commerces dédiés",
                "isValid" => true,
                "position" => 1
            ],
            [
                "name" => "Dispositifs étatiques",
                "isValid" => true,
                "position" => 1
            ],
            [
                "name" => "Centres sociaux",
                "isValid" => true,
                "position" => 1
            ],
            [
                "name" => "Sites internet",
                "isValid" => true,
                "position" => 1
            ],
        ];

        for ($i = 0; $i < count($services); $i++) {
            $service = new Services();
            $service
                ->setName($services[$i]["name"])
                ->setIsValid(true)
                ->setPosition($i);
            $manager->persist($service);
        }


        $manager->flush();
    }
}
