import $ from "jquery";
import "./bootstrap";
import "bootstrap/dist/js/bootstrap";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all";
import "./styles/app.scss";
import AOS from "aos";
AOS.init();

const navbar = $("#navbar");
navbar.addClass("bg-success navbar-dark");

function scrollFunction() {
  if ($(window).width() > 991.98) {
    if (
      document.body.scrollTop > 100 ||
      document.documentElement.scrollTop > 100
    ) {
      // mybutton.css("display","block");
      navbar.removeClass("bg-success navbar-dark");
      console.log("Scroll");
    } else {
      navbar.addClass("bg-success navbar-dark");
    }
  }
}

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function scroll() {
  scrollFunction();
};
